﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using WebService.Codigo;

namespace WebService.App_Code.Clases
{
    public class Rates
    {
        public List<Rate> rates { get; set; }

        public static List<Rate> ObtenerRates()
        {
            List<Rate> rates = new List<Rate>();

            try
            {                
                rates = JsonConvert.DeserializeObject<List<Rate>>(Peticiones.PeticionRate());
            }
            catch(Exception e)
            {
                Logger.Logger.Log(System.Reflection.MethodBase.GetCurrentMethod().Name + " - Error al desserealizar los rates ", Logger.Logger.Nivel.Error);
            }            
            
            return rates;
        }
    }
}