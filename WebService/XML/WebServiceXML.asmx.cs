﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using WS;
using WSRespuestas;

namespace WebService.XML
{
    /// <summary>
    /// Descripción breve de WebServiceXML
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServiceXML : System.Web.Services.WebService
    {

        [WebMethod]
        public RespuestaEstado ObtenerToken(string usuario, string password)
        {
            RespuestaEstado re = Negocio.ObtenerToken(usuario, password);

            return re;
        }
        [WebMethod]
        public RespuestaObtenerRates ObtenerRates(string token)
        {
            RespuestaObtenerRates ror = Negocio.RespuestaObtenerRates(token);

            return ror;
        }
        [WebMethod]
        public RespuestaObtenerTransactions ObtenerTransactions(string token)
        {
            RespuestaObtenerTransactions rot = Negocio.RespuestaObtenerTransactions(token);

            return rot;
        }
        [WebMethod]
        public RespuestaObtenerTransactionsPorSKU RespuestaObtenerTransactionPorSKU(string token, string SKU)
        {
            RespuestaObtenerTransactionsPorSKU rotps = Negocio.RespuestaObtenerTransactionPorSKU(token, SKU);

            return rotps;
        }
    }
}
