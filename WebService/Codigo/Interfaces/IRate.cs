﻿
namespace WebService.App_Code.Interfaces
{
    public interface IRate
    {
        string from { get; set; }
        string to { get; set; }
        string rate { get; set; }
    }
}
