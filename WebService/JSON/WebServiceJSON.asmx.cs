﻿using System;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web.Script.Services;
using System.Web.Services;
using WSRespuestas;
using WS;
using System.ComponentModel;

namespace Servicio
{
    /// <summary>
    /// Descripción breve de WebServiceJSON
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServiceJSON : System.Web.Services.WebService
    {
        
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void ObtenerToken(string usuario, string password)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            RespuestaEstado rl = Negocio.ObtenerToken(usuario, password);

            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(serializer.Serialize(new { RespuestaLogin = rl }));
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void ObtenerRates(string token)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            RespuestaObtenerRates rl = Negocio.RespuestaObtenerRates(token);

            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(serializer.Serialize(new { RespuestaLogin = rl }));
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void ObtenerTransactions(string token)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            RespuestaObtenerTransactions rot = Negocio.RespuestaObtenerTransactions(token);

            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(serializer.Serialize(new { RespuestaLogin = rot }));
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void RespuestaObtenerTransactionPorSKU(string token, string SKU)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            RespuestaObtenerTransactionsPorSKU rot = Negocio.RespuestaObtenerTransactionPorSKU(token, SKU);

            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(serializer.Serialize(new { RespuestaLogin = rot }));
        }
    }
}
