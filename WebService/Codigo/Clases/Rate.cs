﻿using WebService.App_Code.Interfaces;

namespace WebService.App_Code.Clases
{
    public class Rate : IRate
    {
        public string from { get; set; }
        public string to { get; set; }
        public string rate { get; set; }
    }
}