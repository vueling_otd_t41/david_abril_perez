﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using WebService.App_Code.Clases;
using WebService.App_Code.Logger;

namespace WebService.Codigo
{
    public class Peticiones
    {
        public static string PeticionGet(string URL)
        {
            string result = "";
            WebRequest request = WebRequest.Create(URL);
            request.Credentials = CredentialCache.DefaultCredentials;
            WebResponse response = request.GetResponse();
            HttpStatusCode statusCode = ((HttpWebResponse)response).StatusCode;

            if (statusCode.ToString() != "OK")
            {
                Logger.Log(System.Reflection.MethodBase.GetCurrentMethod().Name + " - Error en la peticion " + URL, Logger.Nivel.Error);
            }
            else
            {
                using (Stream dataStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(dataStream);
                    string responseFromServer = reader.ReadToEnd();
                    result = responseFromServer;
                }
            }

            response.Close();
            return result;
        }
        public static string PeticionRate()
        {
            string peticionRate = PeticionGet("http://quiet-stone-2094.herokuapp.com/rates.json");
            if (string.IsNullOrEmpty(peticionRate))
            {
                return Persistencia.Persistencia.RecuperarRates().ToString();
            }
            Persistencia.Persistencia.GuardarRates(JsonConvert.DeserializeObject<List<Rate>>(peticionRate));

            return peticionRate;
        }        
        public static string PeticionTransaction()
        {
            string peticionTransaccion = PeticionGet("http://quiet-stone-2094.herokuapp.com/transactions.json");
            /*
            if (string.IsNullOrEmpty(peticionTransaccion))
            {
                return Persistencia.Persistencia.transactionTemporal;
            }
            Persistencia.Persistencia.GuardarTransactions(peticionTransaccion);
            */
            return peticionTransaccion;
        }
    }
}