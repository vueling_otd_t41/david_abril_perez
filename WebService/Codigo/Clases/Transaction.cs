﻿using WebService.App_Code.Interfaces;

namespace WebService.App_Code.Clases
{
    public class Transaction : ITransaction
    {
        public string sku { get; set; }
        public string amount { get; set; }
        public string currency { get; set; }
    }
}