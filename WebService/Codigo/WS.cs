﻿using System;
using WSRespuestas;
using System.Security.Claims;
using System.Collections.Generic;
using WebService.App_Code.Clases;
using Jwt;
using WebService.App_Code.Logger;
using WebService.Codigo.Persistencia;

namespace WS
{
    public class Negocio
    {
        public static RespuestaEstado ObtenerToken(string usuario, string password)
        {
            RespuestaEstado re = new RespuestaEstado()
            {
                Funcion = System.Reflection.MethodBase.GetCurrentMethod().Name,
                FechaHora = DateTime.Now.ToString(),
                Error = 0,
                MensajeError = "",
                Estado = "Offline"
            };
            Logger.Log(System.Reflection.MethodBase.GetCurrentMethod().Name + " - Inicio ", Logger.Nivel.Info);

            re.Estado = "Online";
            re.JWT = Jwt.JwtManager.GenerateToken(usuario, password, 30);

            Logger.Log(System.Reflection.MethodBase.GetCurrentMethod().Name + " - Fin ", Logger.Nivel.Info); ;

            return re;
        }

        public static RespuestaObtenerRates RespuestaObtenerRates(string token)
        {
            RespuestaObtenerRates re = new RespuestaObtenerRates()
            {
                Funcion = System.Reflection.MethodBase.GetCurrentMethod().Name,
                FechaHora = DateTime.Now.ToString(),
                Error = 0,
                MensajeError = "",
                JWT = token,
                Rates = new List<Rate>()
            };
            Logger.Log(System.Reflection.MethodBase.GetCurrentMethod().Name + " - Inicio ", Logger.Nivel.Info);
            
            try
            {
                if (string.IsNullOrEmpty(token))
                {
                    re.Error = 101;
                    re.MensajeError = "Debe introducir un token";
                }
                else
                {
                    ClaimsPrincipal valorToken = JwtManager.GetPrincipal(token);
                    if (!JwtManager.validarToken(valorToken))
                    {
                        re.Error = 102;
                        re.MensajeError = "Token incorrecto";
                    }
                    else
                    {
                        re.Rates = Rates.ObtenerRates();
                    }
                }
            }
            catch(Exception e)
            {
                Logger.Log(System.Reflection.MethodBase.GetCurrentMethod().Name + " - Error al procesar " + e.StackTrace, Logger.Nivel.Error);
            }

            Logger.Log(System.Reflection.MethodBase.GetCurrentMethod().Name + " - Fin ", Logger.Nivel.Info);

            return re;
        }
        
        public static RespuestaObtenerTransactions RespuestaObtenerTransactions(string token)
        {
            RespuestaObtenerTransactions rot = new RespuestaObtenerTransactions()
            {
                Funcion = System.Reflection.MethodBase.GetCurrentMethod().Name,
                FechaHora = DateTime.Now.ToString(),
                Error = 0,
                MensajeError = "",
                JWT = token,
                Transactions = new List<Transaction>()
            };
            Logger.Log(System.Reflection.MethodBase.GetCurrentMethod().Name + " - Inicio ", Logger.Nivel.Info);
            
            try
            {
                if (string.IsNullOrEmpty(token))
                {
                    rot.Error = 101;
                    rot.MensajeError = "Debe introducir un token";
                }
                else
                {
                    ClaimsPrincipal valorToken = JwtManager.GetPrincipal(token);
                    if (!JwtManager.validarToken(valorToken))
                    {
                        rot.Error = 102;
                        rot.MensajeError = "Token incorrecto";
                    }
                    else
                    {
                        rot.Transactions = Transactions.ObtenerTransactions();
                    }
                }
            }
            catch(Exception e)
            {
                Logger.Log(System.Reflection.MethodBase.GetCurrentMethod().Name + " - Error al procesar " + e.StackTrace, Logger.Nivel.Error);
            }
            

            Logger.Log(System.Reflection.MethodBase.GetCurrentMethod().Name + " - Fin ", Logger.Nivel.Info);

            return rot;
        }

        public static RespuestaObtenerTransactionsPorSKU RespuestaObtenerTransactionPorSKU(string token, string SKU)
        {
            RespuestaObtenerTransactionsPorSKU rotps = new RespuestaObtenerTransactionsPorSKU()
            {
                Funcion = System.Reflection.MethodBase.GetCurrentMethod().Name,
                FechaHora = DateTime.Now.ToString(),
                Error = 0,
                MensajeError = "",
                JWT = token,
                Transactions = new List<Transaction>()
            };
            Logger.Log(System.Reflection.MethodBase.GetCurrentMethod().Name + " - Inicio ", Logger.Nivel.Info);
            
            try
            {
                if (string.IsNullOrEmpty(token))
                {
                    rotps.Error = 101;
                    rotps.MensajeError = "Debe introducir un token";
                }
                else
                {
                    ClaimsPrincipal valorToken = JwtManager.GetPrincipal(token);
                    if (!JwtManager.validarToken(valorToken))
                    {
                        rotps.Error = 102;
                        rotps.MensajeError = "Token incorrecto";
                    }
                    else
                    {
                        rotps.Transactions = Transactions.ObtenerTransactionsPorSKU(SKU);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Log(System.Reflection.MethodBase.GetCurrentMethod().Name + " - Error al procesar " + e.StackTrace, Logger.Nivel.Error);
            }

            Logger.Log(System.Reflection.MethodBase.GetCurrentMethod().Name + " - Fin ", Logger.Nivel.Info);

            return rotps;
        }
    }
}