﻿using System;
using System.Collections.Generic;
using WebService.App_Code.Clases;

namespace WSRespuestas
{
    public class RespuestaBase
    {
        public string Funcion { get; set; }
        public string FechaHora = DateTime.Now.ToString();
        public int Error { get; set; }
        public string MensajeError { get; set; }
        public string JWT { get; set; }
    }

    public class RespuestaEstado : RespuestaBase
    {
        public string Estado { get; set; }
    }

    public class RespuestaObtenerRates : RespuestaBase
    {
        public List<Rate> Rates { get; set; }
    }

    public class RespuestaObtenerTransactions : RespuestaBase
    {
        public List<Transaction> Transactions { get; set; }
    }

    public class RespuestaObtenerTransactionsPorSKU : RespuestaBase
    {
        public List<Transaction> Transactions { get; set; }
    }
}