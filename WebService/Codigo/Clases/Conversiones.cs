﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebService.App_Code.Clases;
using WebService.App_Code.Logger;

namespace WebService.Codigo.Clases
{
    public class Conversiones
    {
        public static List<Rate> ConvertirRates(List<Rate> rates)
        {
            List<Rate> ratesCompletos = new List<Rate>();

            foreach (Rate rateFrom in rates)
            {
                bool existeRate = false;
                foreach (Rate rateTo in rates)
                {
                    if (rateFrom.from == rateTo.to && rateFrom.to == rateTo.from)
                    {
                        existeRate = true;
                    }
                }
                if (!existeRate)
                {
                    ratesCompletos.Add(rateFrom);
                    Rate rateMapeado = MapearRate(rateFrom, rates);
                    if (rateMapeado != null)
                    {
                        ratesCompletos.Add(rateMapeado);
                    }
                }
                else
                {
                    ratesCompletos.Add(rateFrom);
                }
            }

            return ratesCompletos;
        }

        private static Rate MapearRate(Rate rate, List<Rate> rates)
        {
            List<Rate> ratesTemp = new List<Rate>(rates);
            ratesTemp.Remove(rate);
            Rate rateMapeado = null;
            decimal valorRate = 0;
            try
            {
                valorRate = Convert.ToDecimal(Math.Round(Convert.ToDouble(rate.rate)));
                decimal rateFromTo = 0;
                decimal rateToFrom = 0;
                decimal rateIntermedio = 0;
                foreach (Rate rateFrom in ratesTemp)
                {
                    foreach (Rate rateTo in ratesTemp)
                    {
                        if (rateFrom.from == rate.from)
                        {
                            rateToFrom = Convert.ToDecimal(rateFrom.rate.Replace(".", ","));
                        }
                        else if (rateFrom.to == rate.from)
                        {
                            rateFromTo = Convert.ToDecimal(rateFrom.rate.Replace(".", ","));
                        }
                    }
                    if (rateFromTo != 0 && rateToFrom != 0)
                    {
                        rateIntermedio = RoundHalfToEven((rateToFrom * rateFromTo) / valorRate);
                    }
                }
                if (rateFromTo != 0 && rateToFrom != 0 && rateIntermedio != 0)
                {
                    rateMapeado = new Rate();
                    rateMapeado.from = rate.to;
                    rateMapeado.to = rate.from;
                    rateMapeado.rate = rateIntermedio.ToString().Replace(",", ".");
                }
            } catch(Exception e) { 
                Logger.Log(System.Reflection.MethodBase.GetCurrentMethod().Name + " - Error al convertir el rate ", Logger.Nivel.Error);
            }

            return rateMapeado;
        }

        public static List<Transaction> ConvertirTransacciones(List<Transaction> transactions, List<Rate> ratesConvertidos)
        {
            foreach(Transaction transaccion in transactions)
            {
                if (transaccion.currency != "EUR")
                {
                    decimal amount = Convert.ToDecimal(transaccion.amount.Replace(".",","));
                    string currency = transaccion.currency;
                    bool encontrado = false;
                    foreach (Rate rate in ratesConvertidos)
                    {
                        if (rate.from == currency && rate.to == "EUR")
                        {
                            encontrado = true;
                            transaccion.amount = RoundHalfToEven((amount * Convert.ToDecimal(rate.rate.Replace(".", ",")))).ToString().Replace(",", ".");
                            transaccion.currency = "EUR";
                        }
                    }
                    if (!encontrado)
                    {
                        foreach (Rate rate in ratesConvertidos)
                        {
                            if (transaccion.currency == rate.from)
                            {
                                foreach (Rate rateInterno in ratesConvertidos)
                                {
                                    if (rate.to == rateInterno.from && rateInterno.to == "EUR")
                                    {
                                        transaccion.amount = RoundHalfToEven((amount * Convert.ToDecimal(rate.rate.Replace(".", ",")) * Convert.ToDecimal(rateInterno.rate.Replace(".", ",")))).ToString().Replace(",", ".");
                                        transaccion.currency = "EUR";
                                    }
                                }
                            }                                                 
                        }
                    }
                }
            }

            return transactions;
        }

        public static decimal RoundHalfToEven(decimal valor)
        {
            return Math.Round(Math.Truncate(valor * 1000) / 1000, 2, MidpointRounding.ToEven);
        }
    }
}