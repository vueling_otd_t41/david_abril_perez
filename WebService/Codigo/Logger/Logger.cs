﻿using log4net;

namespace WebService.App_Code.Logger
{
    public static class Logger
    {
        private static ILog _logger;
        public enum Nivel { Info, Error};

        public static void Inicializar(ILog logger)
        {
            _logger = logger;
        }

        public static void Log(string logEntrada, Nivel nivel)
        {
            if (_logger == null)
                _logger = LogManager.GetLogger("LogFileAppender");

            if (nivel == Nivel.Info)
            {
                _logger.Info(logEntrada);
            }else if (nivel == Nivel.Error)
            {
                _logger.Error(logEntrada);
            }
        }
    }
}