﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using WebService.Codigo;
using WebService.Codigo.Clases;

namespace WebService.App_Code.Clases
{
    public class Transactions
    {
        public List<Transaction> transactions { get; set; }

        public static List<Transaction> ObtenerTransactions()
        {
            List<Transaction> transactions = new List<Transaction>();

            try
            {
                transactions = JsonConvert.DeserializeObject<List<Transaction>>(Peticiones.PeticionTransaction());
            }
            catch (Exception e)
            {
                Logger.Logger.Log(System.Reflection.MethodBase.GetCurrentMethod().Name + " - Error al desserealizar las transactions ", Logger.Logger.Nivel.Error);
            }

            return transactions;
        }
        
        public static List<Transaction> ObtenerTransactionsPorSKU(string SKU)
        {
            List<Rate> rates = new List<Rate>();
            List<Transaction> transactions = new List<Transaction>();

            try
            {
                rates = JsonConvert.DeserializeObject<List<Rate>>(Peticiones.PeticionRate());
                rates = Conversiones.ConvertirRates(rates);
                transactions = JsonConvert.DeserializeObject<List<Transaction>>(Peticiones.PeticionTransaction());
                transactions = transactions.Where(x => x.sku == SKU).ToList();
                transactions = Conversiones.ConvertirTransacciones(transactions, rates);
            }
            catch (Exception e)
            {
                Logger.Logger.Log(System.Reflection.MethodBase.GetCurrentMethod().Name + " - Error al desserealizar las transactions ", Logger.Logger.Nivel.Error);
            }

            return transactions;
        }
    }
}