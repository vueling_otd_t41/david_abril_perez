﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using WebService.App_Code.Clases;
using WebService.App_Code.Logger;

namespace WebService.Codigo.Persistencia
{
    public class Persistencia
    {
        private static string directorioProyecto = AppDomain.CurrentDomain.BaseDirectory;
        private static string rutaRates = directorioProyecto + "\\Ficheros\\rates\\rates.json";
        private static string rutaTransactions = directorioProyecto + "\\Ficheros\\transactions\\transactions.json";

        public static void GuardarRates(List<Rate> rates)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(rutaRates, false))
                {
                    sw.WriteLine(JsonConvert.SerializeObject(rates));
                    sw.Close();
                }
            }
            catch (Exception e)
            {
                Logger.Log("Error al guardar el fichero de rates.json " + e.StackTrace, Logger.Nivel.Error);
            }

        }
        public static List<Rate> RecuperarRates()
        {
            List<Rate> rates = new List<Rate>();

            try
            {
                if (File.Exists(rutaRates))
                {
                    StreamReader file = new StreamReader(rutaRates);
                    string contenido = File.ReadAllText(rutaRates);
                    rates = JsonConvert.DeserializeObject<List<Rate>>(contenido);
                }
            }
            catch (Exception e)
            {
                Logger.Log("Error al recuperar el fichero de rates.json " + e.StackTrace, Logger.Nivel.Error);
            }

            return rates;
        }
        public static void GuardarTransactions(List<Transaction> transactions)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(rutaTransactions, false))
                {
                    sw.WriteLine(JsonConvert.SerializeObject(transactions));
                    sw.Close();
                }
            }
            catch (Exception e)
            {
                Logger.Log("Error al guardar el fichero de transactions.json " + e.StackTrace, Logger.Nivel.Error);
            }

        }
        public static List<Transaction> RecuperarTransactions()
        {
            List<Transaction> transactions = new List<Transaction>();

            try
            {
                if (File.Exists(rutaTransactions))
                {
                    StreamReader file = new StreamReader(rutaTransactions);
                    string contenido = File.ReadAllText(rutaTransactions);
                    transactions = JsonConvert.DeserializeObject<List<Transaction>>(contenido);
                }
            }
            catch (Exception e)
            {
                Logger.Log("Error al recuperar el fichero de transactions.json " + e.StackTrace, Logger.Nivel.Error);
            }

            return transactions;
        }
    }
}