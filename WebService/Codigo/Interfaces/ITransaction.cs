﻿
namespace WebService.App_Code.Interfaces
{
    public interface ITransaction
    {
        string sku { get; set; }
        string amount { get; set; }
        string currency { get; set; }
    }
}
