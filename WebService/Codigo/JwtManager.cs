﻿using System;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;

namespace Jwt
{
    public static class JwtManager
    {
        private static string Secret = ConfigurationManager.AppSettings["Secret"];
        private static int defautlExpireMinutes = Convert.ToInt32(ConfigurationManager.AppSettings["expireMinutes"]);

        public static string GenerateToken(string username, string password, int expireMinutes)
        {
            expireMinutes = (expireMinutes == 0) ? defautlExpireMinutes : expireMinutes;
            var symmetricKey = Convert.FromBase64String(Secret);
            var tokenHandler = new JwtSecurityTokenHandler();

            var now = DateTime.UtcNow;
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                        {
                            new Claim("usuario", username),
                            new Claim("password", password)
                        }),

                Expires = now.AddMinutes(Convert.ToInt32(expireMinutes)),

                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(symmetricKey), SecurityAlgorithms.HmacSha256Signature)
            };

            SecurityToken securityToken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(securityToken);

            return token;
        }

        public static ClaimsPrincipal GetPrincipal(string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var jwtToken = tokenHandler.ReadToken(token) as JwtSecurityToken;

                if (jwtToken == null)
                    return null;

                var symmetricKey = Convert.FromBase64String(Secret);

                var validationParameters = new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = new SymmetricSecurityKey(symmetricKey)
                };

                var principal = tokenHandler.ValidateToken(token, validationParameters, out _);                

                return principal;
            }

            catch (Exception)
            {
                return null;
            }
        }

        public static bool validarToken(ClaimsPrincipal valorToken)
        {
            return valorToken != null && valorToken.Identity.IsAuthenticated;
        }
    }
}